import * as axios from 'axios';
import * as JsonAdapter from 'axios-jsonp';
import { throwError } from 'rxjs';


export class WikiSerice {
  constructor() {
    this.url = 'https://en.wikipedia.org/w/api.php';
    this.axios = axios.create({
      baseURL: this.url,
      adapter: JsonAdapter
    })
  }  
  
  search (sentence) {
    if(sentence && typeof sentence == 'string' && sentence != 'error') {
      return this.axios.get('', { 
        params: {
          search: sentence,
          action: 'opensearch',
          format: 'json'
        }
      });
    } else if (sentence === 'error'){
      return throwError('Simulate Error from wikipedia')
    } else {
      throw Error(`Type's sentence must be string`);
    }
  }

}
