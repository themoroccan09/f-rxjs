import { Observable } from 'rxjs';
import { share, shareReplay } from 'rxjs/operators';
const resultUl = document.getElementById('result');


const cold$ = Observable.create(obs => {
  obs.next(1);
  obs.next(2);
  obs.next(3);
  console.log('Observable played')
  setTimeout(() => obs.next(4), 1000)
});


const convertedObs$ = cold$.pipe(shareReplay(1))
/* Only receive 3 and 4 */
convertedObs$.subscribe(val => console.log(`sub1 converted value ${val}`))
convertedObs$.subscribe(val => console.log(`sub2 converted value ${val}`))



function clearChildren(el) {
  while(el.firstChild) {
    el.removeChild(el.firstChild)
  }
}
function addToResult(value) {
  var li = document.createElement('li');
  li.className = "list-group-item";
  li.innerText = value;
  resultUl.appendChild(li)
}

